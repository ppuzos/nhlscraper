const nhlApi = require("nhl-api");
const fs = require("fs");

const nhlplayerstats = fs.createWriteStream("./nhlplayerstats.json");

const fetchData = cb =>
  nhlApi.team({ expand: ["team.roster", "person.stats"] }).then(async teams => {
    const players = [];
    await Promise.all(
      teams.map(async team => {
        await Promise.all(
          team.roster.roster.map(async player => {
            try {
              await nhlApi
                .people(player.person.id, {
                  expand: ["person.stats"],
                  season: "20192020",
                  stats: "statsSingleSeason"
                })
                .then(player => {
                  if (player) {
                    players.push(player);
                  }
                });
            } catch (e) {
              console.log(e);
            }
          })
        );
      })
    );
    nhlplayerstats.write(JSON.stringify(players));
  });

fetchData(() => nhlplayerstats.close());
