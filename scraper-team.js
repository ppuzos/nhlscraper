const nhlApi = require("nhl-api");
const fs = require("fs");

const nhlteamstats = fs.createWriteStream("./nhlteamstats.json");

const fetchData = async cb => {
  try {
    await nhlApi.team({ expand: "team.stats" }).then(teams => {
      nhlteamstats.write(JSON.stringify(teams));
    });
  } catch (e) {
    console.log(e);
  }
};

fetchData(() => nhlteamstats.close());
