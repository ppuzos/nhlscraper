const fs = require("fs"),
  axios = require("axios");

const dir = process.argv[2];
const uri =
  "http://www-league.nhlstatic.com/images/logos/teams-current-circle/";

const download = (url, image_path, cb) =>
  axios({ url: url, responseType: "stream" })
    .then(response => {
      console.log(image_path);
      response.data.pipe(fs.createWriteStream(image_path));

      return { status: true, error: "" };
    })
    .catch(error =>
      cb({ url, status: false, error: "Error: " + error.message })
    );

var i;
for (i = 1; i < 31; i++) {
  var file = `${i}.svg`;
  download(`${uri}${file}`, `${dir}${file}`, error => console.log(error));
}
